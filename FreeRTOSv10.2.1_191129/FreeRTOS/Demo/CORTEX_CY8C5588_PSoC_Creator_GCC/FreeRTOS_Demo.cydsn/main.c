/*
 * FreeRTOS Kernel V10.2.1
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

#include <device.h>
#include "project.h"
#include <stdio.h>
#include "FreeRTOS.h" /* RTOS includes. */
#include "task.h"

#define ENC_TASK_PRIORTIY       (tskIDLE_PRIORITY)
#define POLL_TASK_PRIORITY      (tskIDLE_PRIORITY + 2)
#define TOL_TASK_PRIORITY       (tskIDLE_PRIORITY)
#define MODE_TASK_PRIORITY      (tskIDLE_PRIORITY)
#define SPEED_TASK_PRIORITY     (tskIDLE_PRIORITY) 
#define TOUT_TASK_PRIORITY      (tskIDLE_PRIORITY + 1)
#define UPDATE_TASK_PRIORITY    (tskIDLE_PRIORITY + 3)
#define MOTOR_TASK_PRIORITY     (tskIDLE_PRIORITY + 4)  //highest priority task to keep the motor turning

#define OFF     0
#define COOLING 1
#define HEATING 2
#define LOW     3
#define MEDIUM  4
#define HIGH    5

#define MIN_TEMP_SETPOINT   15
#define MAX_TEMP_SETPOINT   40

#define DELAY_TIME  200

volatile int awake_flag =  0;
int awake = 1;
char disp[5];
char disp2[5];
int mode;
uint8 setpoint;
int speed;
int animate;
uint8 TOL;
uint8 TH;
uint8 TL;
uint8 slave_address = 0x48;
char tempBuf[9];
uint16 delay_time = 20;

void temp_initialize(uint8 slave_address)
{
        uint8 Buffer1[]= {0xAC,0x02};           //sets polarity
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer1,2, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer2[]= {0xA1,0x37,0x80};      //sets TH limit to 25.5 degrees
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer2,3, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer3[]= {0xA2,0x36,0x80};      //sets the TL limit to 24.5 degrees
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer3,3, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer4[]= {0xEE};                //starts convert T command
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer4,1, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
}

void read_temp(uint8 slave_address, char* dataArray)
{
    char disp[17];
    memset(dataArray, 0, 9);                                         //clear string before saving read data to it
    uint8 command[1];
    command[0] = 0xAA;                                              //read current temp command

    I2C_MasterWriteBuf((uint8)slave_address,command,1, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
    
    I2C_MasterReadBuf((uint8)slave_address,(uint8*)dataArray,2,I2C_MODE_COMPLETE_XFER);     //gets the 2 bytes of temperature data
    while (!(I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT));
    I2C_MasterClearStatus();

    if (dataArray[1] == 128) dataArray[1] = 5;                                              //if the 2nd byte is 0.5C, make it a 5 to display on LCD
    sprintf(disp, "Temp: %2d.%d",dataArray[0],dataArray[1]);                                //mashing together the integers to display on LCD easily
    LCD_Position(0,0);
    LCD_PrintString(disp);
}

void write_deadband(uint8 slave_address, uint8 set, uint8 tol)
{
    uint8 command[3];
    TH = set + tol/2;
    TL = set - tol/2;
    
    command[0] = 0xA1;        
    command[1] = TH;
    command[2] = (tol % 2) == 0 ? 0x00 : 0x80;                  //check if there's a decimal which is 0.5 degrees C which is 8 bits 128 decimal value or 0x80 
    I2C_MasterWriteBuf((uint8)slave_address, command,3, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
 
    command[0] = 0xA2;        
    command[1] = TL;
    command[2] = (tol % 2) == 0 ? 0x00 : 0x80;
    I2C_MasterWriteBuf((uint8)slave_address, command,3, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
}

void access_config(uint8 slave_address, uint8 setting)
{
    uint8 command[2];
    command[0] = 0xAC;
    I2C_MasterWriteBuf((uint8)slave_address,command,1, I2C_MODE_COMPLETE_XFER);     //write the command bit first & then read
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
    
    command[0] = 0xAC;                                                              //access the config register by writing command
    command[1] = setting;                                                           //write setting (active high or low) to config register
    I2C_MasterWriteBuf((uint8)slave_address,command,2, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
}

/*---------------------------------------------------------------------------*/
void vUpdate(void *pvParameters);
void vMotor(void *pvParameters);
void vSpeed(void *pvParameters);
void vMode(void *pvParameters);
void vTOut(void *pvParameters);
void vTOL(void *pvParameters);
void vPoll(void *pvParameters);
void vEnc(void *pvParameters);
TaskHandle_t UpdateTaskHandle = NULL;
TaskHandle_t MotorTaskHandle = NULL;
TaskHandle_t SpeedTaskHandle = NULL;
TaskHandle_t ModeTaskHandle = NULL;
TaskHandle_t TOutTaskHandle = NULL;
TaskHandle_t TOLTaskHandle = NULL;
TaskHandle_t PollTaskHandle = NULL;
TaskHandle_t EncTaskHandle = NULL;

/*---------------------------------------------------------------------------*/
/*
 * Installs the RTOS interrupt handlers and starts the peripherals.
 */
static void prvHardwareSetup( void );
/*---------------------------------------------------------------------------*/
void main( void )
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	    prvHardwareSetup();
        
	/* Start the standard demo tasks.  These are just here to exercise the
	kernel port and provide examples of how the FreeRTOS API can be used. */
    xTaskCreate(vUpdate,"Update",configMINIMAL_STACK_SIZE,(void*) 0, UPDATE_TASK_PRIORITY, &UpdateTaskHandle);
    xTaskCreate(vMotor,"Motor",configMINIMAL_STACK_SIZE,(void*) 0,MOTOR_TASK_PRIORITY,&MotorTaskHandle);
    xTaskCreate(vSpeed,"Motor",configMINIMAL_STACK_SIZE,(void*) 0, SPEED_TASK_PRIORITY,&SpeedTaskHandle);
    xTaskCreate(vMode,"Mode",configMINIMAL_STACK_SIZE,(void*) 0, MODE_TASK_PRIORITY,&ModeTaskHandle);
    xTaskCreate(vTOut,"TOut",configMINIMAL_STACK_SIZE,(void*) 0, TOUT_TASK_PRIORITY, &TOutTaskHandle);
    xTaskCreate(vPoll,"Poll",configMINIMAL_STACK_SIZE, (void*) 0, POLL_TASK_PRIORITY,&PollTaskHandle);
    xTaskCreate(vEnc,"Enc",configMINIMAL_STACK_SIZE,(void*) 0, ENC_TASK_PRIORTIY,&EncTaskHandle);  

	/* Will only get here if there was insufficient memory to create the idle
    task.  The idle task is created within vTaskStartScheduler(). */
	vTaskStartScheduler();

	/* Should never reach here as the kernel will now be running.  If
	vTaskStartScheduler() does return then it is very likely that there was
	insufficient (FreeRTOS) heap space available to create all the tasks,
	including the idle task that is created within vTaskStartScheduler() itself. */
	for( ;; );
}
/*---------------------------------------------------------------------------*/

void prvHardwareSetup( void )
{
/* Port layer functions that need to be copied into the vector table. */
extern void xPortPendSVHandler( void );
extern void xPortSysTickHandler( void );
extern void vPortSVCHandler( void );
extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = ( cyisraddress ) vPortSVCHandler;
	CyRamVectors[ 14 ] = ( cyisraddress ) xPortPendSVHandler;
	CyRamVectors[ 15 ] = ( cyisraddress ) xPortSysTickHandler;
    
    /*Initialize Variables & Start Peripherals */
    speed = LOW;
    mode = OFF;
    TOL = 1;
    setpoint = 25;
    
    CyGlobalIntEnable; /* Enable global interrupts. */
    Timer_48MHz_Start();
    QuadDec_Start();
    LCD_Start();
    I2C_Start();
    POWER_ISR_Start();
    QuadDec_SetCounter(setpoint);
    temp_initialize(slave_address);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void vUpdate(void *pvParameters){ //Update the LCD with values
    TickType_t xLastWakeTime;

    for(;;){
        taskENTER_CRITICAL();
            switch(speed)
            {
                case LOW:
                    sprintf(disp,"LOW ");
                    break;
                case MEDIUM:
                    sprintf(disp,"MED ");
                    break;
                case HIGH:
                    sprintf(disp,"HIGH");
                    break;
                default:
                    sprintf(disp,"LOW ");
                    break;
            }
            switch(mode)
            {
                case COOLING:
                    sprintf(disp2,"COOL");
                    break;
                case HEATING:
                    sprintf(disp2,"HEAT");
                    break;
                default:
                    sprintf(disp2,"OFF ");
                    break;   
            }
            if ((mode != OFF) && Tout_Read() == 1)
            { 
                switch(animate)
                {
                    case 0:
                        LCD_Position(1,5);
                        //LCD_PrintString("|");
                        LCD_PutChar(LCD_CUSTOM_0);
                        animate = 1;
                        break;
                    case 1:
                        LCD_Position(1,5);
                        //LCD_PrintString("/");
                        LCD_PutChar(LCD_CUSTOM_2);
                        animate = 2;     
                        break;
                    case 2:
                        LCD_Position(1,5);
                        //LCD_PrintString("-");
                        LCD_PutChar(LCD_CUSTOM_1);
                        animate = 3;
                        break;
                    case 3:
                        LCD_Position(1,5);
                        LCD_PutChar(LCD_CUSTOM_3);
                        animate = 0;
                        break;
                    default:
                        break;
                }
            }
            else LCD_ClearDisplay(); 
            read_temp(slave_address,tempBuf);
            LCD_Position(0,12);
            LCD_PrintString(disp);
            LCD_Position(1,0);
            LCD_PrintNumber(setpoint);
            LCD_Position(1,8);
            LCD_PrintNumber(TOL);
            LCD_Position(1,12);
            LCD_PrintString(disp2);        
        taskEXIT_CRITICAL();
        xLastWakeTime = xTaskGetTickCount();
        vTaskDelayUntil(&xLastWakeTime,DELAY_TIME/portTICK_PERIOD_MS); //every 0.2 seconds this task is called to update the screen
    }
}
    
void vMotor(void *pvParameters){    //This is the loop that turns the motor depending on conditions
    int state = 0;
    TickType_t xLastWakeTime;

    for(;;)
    { 
        taskENTER_CRITICAL();
            state = MOTOR_Read();
            switch(mode)
            {
            case HEATING:
                switch(state)           //sequence for cw rotation
                {
                    case 0xA: 
                        MOTOR_Write(0x9);
                        break;
                    case 0x9:
                        MOTOR_Write(0x5);
                        break;
                    case 0x5:
                        MOTOR_Write(0x6);
                        break;
                    case 0x6:
                        MOTOR_Write(0xA);
                        break;
                    default:
                        MOTOR_Write(0xA);
                        break;
                }
                break;
            case COOLING:
                switch(state)               //sequence for ccw rotation
                {
                    case 0xA: 
                        MOTOR_Write(0x6);
                        break;
                    case 0x9:
                        MOTOR_Write(0xA);
                        break;
                    case 0x5:
                        MOTOR_Write(0x9);
                        break;
                    case 0x6:
                        MOTOR_Write(0x5);
                        break;
                    default:
                        MOTOR_Write(0xA);
                        break;                
                }
                break;
            case OFF:
                MOTOR_Write(0);
                break;
            default:
                MOTOR_Write(0);
                break;
            }
        taskEXIT_CRITICAL();
        xLastWakeTime = xTaskGetTickCount();
        vTaskDelayUntil(&xLastWakeTime,delay_time/portTICK_PERIOD_MS); 
        //makes sure the motor spins at the right speed by calling the task at the right period
    }
}

void vSpeed(void *pvParameters){        //controls the period at which the vMotor task will be called & context switch
    for(;;){
        vTaskSuspend(NULL);             //suspend task until speed button is pressed in polling task
        taskENTER_CRITICAL();
            switch(speed)
            {
                case LOW:
                    speed = MEDIUM;
                    delay_time = 10;
                    break;
                case MEDIUM:
                    speed = HIGH;
                    delay_time = 5;
                    break;
                case HIGH:
                    speed = LOW;
                    delay_time = 20;
                    break;
                default:
                    speed = LOW;
                    delay_time = 20;
                    break;
            }
        taskEXIT_CRITICAL();
    }
}

void vMode(void *pvParameters){ //must have higher priority if going to resume TOut Task? & context switch back to polling task
    uint8 POL;
    for(;;){
        vTaskSuspend(NULL);     //suspend until mode button pressed in polling task
        taskENTER_CRITICAL();
            switch(mode)
            {
                case OFF:
                    mode = COOLING;
                    POL = 2;                    //set polarity bit to 1 for cooling in config register
                    write_deadband(slave_address, setpoint, TOL);
                    access_config(slave_address,POL);       
                    break;
                case COOLING:
                    mode = HEATING;
                    POL = 0;                    //set polarity bit to 0 for heating
                    write_deadband(slave_address, setpoint, TOL);
                    access_config(slave_address,POL);
                    break;
                case HEATING:
                    mode = OFF;
                    MOTOR_Write(0);
                    break;
                default:
                    mode = OFF;
                    MOTOR_Write(0);
                    break;
            }
        taskEXIT_CRITICAL();
    }
}

void vTOut(void *pvParameters){                 //poll TOut and resume Motor task if necessary & context switch back to polling task
    TickType_t xLastWakeTime;
    
    for(;;){
        if (Tout_Read() == 0){
            MOTOR_Write(0);                         //stop the motor to keep it from getting hot
            vTaskSuspend(MotorTaskHandle);
        }
        else vTaskResume(MotorTaskHandle);          

        xLastWakeTime = xTaskGetTickCount();
        vTaskDelayUntil(&xLastWakeTime,100/portTICK_PERIOD_MS); //call task to read tout pin every 0.1 seconds
    }
}

void vTOL(void *pvParameters){                              //context switch back to polling task
    for(;;){
        vTaskSuspend(NULL);                                 //suspend until TOL button pressed in polling task
        taskENTER_CRITICAL();                               //want to make sure I2C completes
            write_deadband(slave_address, setpoint, TOL);   //update TH/TL on the temp chip
        taskEXIT_CRITICAL();
    }
}

void vEnc(void *pvParameters){
    for(;;){
        vTaskSuspend(NULL);
        taskENTER_CRITICAL();
            write_deadband(slave_address, setpoint, TOL);   //update TH/TL on the temp chip
        taskEXIT_CRITICAL();
    }
}

void vPoll(void *pvParameters){ //this is the main loop that polls the buttons
    TickType_t xLastWakeTime;
    uint8 speed_value = 0;
    uint8 mode_value = 0;
    uint8 tol_value = 0;
    
    for(;;){
        taskENTER_CRITICAL();
            speed_value = SPEED_Read();
            mode_value = MODE_Read();
            tol_value = TOL_Read();
            setpoint = QuadDec_GetCounter();
            CyDelayUs(50); //debounce the buttons
            if ((speed_value == SPEED_Read()) && speed_value == 1){
                vTaskResume(SpeedTaskHandle);               //allow the task to be scheduled wrt priority level
            }
            if ((mode_value == MODE_Read()) && mode_value == 1){
                vTaskResume(ModeTaskHandle);
            }
            if ((tol_value == TOL_Read()) && tol_value == 1){
                TOL++;
                if (TOL == 6) TOL = 1;                          //range is 1C to 5C so loop back around (increments of 1) 
                vTaskResume(TOLTaskHandle);
            }
            if (setpoint == QuadDec_GetCounter()) {
                if (setpoint >= 40) QuadDec_SetCounter(MAX_TEMP_SETPOINT);
                else if (setpoint <= 15) QuadDec_SetCounter(MIN_TEMP_SETPOINT);
                vTaskResume(EncTaskHandle);
            }
        taskEXIT_CRITICAL();
        xLastWakeTime = xTaskGetTickCount();
        vTaskDelayUntil(&xLastWakeTime,100/portTICK_PERIOD_MS); //call poll task to read buttons every 0.1 seconds
    }
}

void vApplicationSleep (TickType_t xExpectedIdleTime)
{
    eSleepModeStatus eSleepStatus;
    Timer_48MHz_Sleep();
    
    eSleepStatus = eTaskConfirmSleepModeStatus();
    if (eSleepStatus == eAbortSleep)
    {
        Timer_48MHz_Wakeup();
//        LCD_ClearDisplay();
//        LCD_PrintString("eAbortSleep");
    }
    else
    {
        if (eSleepStatus == eNoTasksWaitingTimeout)
        {
//            LCD_ClearDisplay();
//            LCD_PrintString("eNoTasks");
            I2C_Sleep();
            LCD_Sleep();
            QuadDec_Sleep();
            CyPmSaveClocks();
            CyPmHibernate(); //makes us have hardware interrupt to wakeup system
        }
        else
        {
            if (awake_flag == 1)
            {
//                LCD_ClearDisplay();
//                LCD_PrintString("flag");
                awake_flag = 0;
                
                if (awake == 1)
                {
//                    LCD_ClearDisplay();
//                    LCD_PrintString("sleep");
                    awake = 0;
                    LCD_Sleep();
                    I2C_Sleep();
                    QuadDec_Sleep();
                    CyPmSaveClocks();
                    CyPmHibernate();
                }
                else
                {
                    LCD_Wakeup();
//                    LCD_ClearDisplay();
//                    LCD_PrintString("awake");
                    I2C_Wakeup();
                    QuadDec_Wakeup();
                    CyPmRestoreClocks();
                    Timer_48MHz_Wakeup();
                    awake = 1;
                }
            }
        }
    }
}
/*******************************************************************************
* File Name: POWER.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_POWER_H) /* Pins POWER_H */
#define CY_PINS_POWER_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "POWER_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 POWER__PORT == 15 && ((POWER__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    POWER_Write(uint8 value);
void    POWER_SetDriveMode(uint8 mode);
uint8   POWER_ReadDataReg(void);
uint8   POWER_Read(void);
void    POWER_SetInterruptMode(uint16 position, uint16 mode);
uint8   POWER_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the POWER_SetDriveMode() function.
     *  @{
     */
        #define POWER_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define POWER_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define POWER_DM_RES_UP          PIN_DM_RES_UP
        #define POWER_DM_RES_DWN         PIN_DM_RES_DWN
        #define POWER_DM_OD_LO           PIN_DM_OD_LO
        #define POWER_DM_OD_HI           PIN_DM_OD_HI
        #define POWER_DM_STRONG          PIN_DM_STRONG
        #define POWER_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define POWER_MASK               POWER__MASK
#define POWER_SHIFT              POWER__SHIFT
#define POWER_WIDTH              1u

/* Interrupt constants */
#if defined(POWER__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in POWER_SetInterruptMode() function.
     *  @{
     */
        #define POWER_INTR_NONE      (uint16)(0x0000u)
        #define POWER_INTR_RISING    (uint16)(0x0001u)
        #define POWER_INTR_FALLING   (uint16)(0x0002u)
        #define POWER_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define POWER_INTR_MASK      (0x01u) 
#endif /* (POWER__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define POWER_PS                     (* (reg8 *) POWER__PS)
/* Data Register */
#define POWER_DR                     (* (reg8 *) POWER__DR)
/* Port Number */
#define POWER_PRT_NUM                (* (reg8 *) POWER__PRT) 
/* Connect to Analog Globals */                                                  
#define POWER_AG                     (* (reg8 *) POWER__AG)                       
/* Analog MUX bux enable */
#define POWER_AMUX                   (* (reg8 *) POWER__AMUX) 
/* Bidirectional Enable */                                                        
#define POWER_BIE                    (* (reg8 *) POWER__BIE)
/* Bit-mask for Aliased Register Access */
#define POWER_BIT_MASK               (* (reg8 *) POWER__BIT_MASK)
/* Bypass Enable */
#define POWER_BYP                    (* (reg8 *) POWER__BYP)
/* Port wide control signals */                                                   
#define POWER_CTL                    (* (reg8 *) POWER__CTL)
/* Drive Modes */
#define POWER_DM0                    (* (reg8 *) POWER__DM0) 
#define POWER_DM1                    (* (reg8 *) POWER__DM1)
#define POWER_DM2                    (* (reg8 *) POWER__DM2) 
/* Input Buffer Disable Override */
#define POWER_INP_DIS                (* (reg8 *) POWER__INP_DIS)
/* LCD Common or Segment Drive */
#define POWER_LCD_COM_SEG            (* (reg8 *) POWER__LCD_COM_SEG)
/* Enable Segment LCD */
#define POWER_LCD_EN                 (* (reg8 *) POWER__LCD_EN)
/* Slew Rate Control */
#define POWER_SLW                    (* (reg8 *) POWER__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define POWER_PRTDSI__CAPS_SEL       (* (reg8 *) POWER__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define POWER_PRTDSI__DBL_SYNC_IN    (* (reg8 *) POWER__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define POWER_PRTDSI__OE_SEL0        (* (reg8 *) POWER__PRTDSI__OE_SEL0) 
#define POWER_PRTDSI__OE_SEL1        (* (reg8 *) POWER__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define POWER_PRTDSI__OUT_SEL0       (* (reg8 *) POWER__PRTDSI__OUT_SEL0) 
#define POWER_PRTDSI__OUT_SEL1       (* (reg8 *) POWER__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define POWER_PRTDSI__SYNC_OUT       (* (reg8 *) POWER__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(POWER__SIO_CFG)
    #define POWER_SIO_HYST_EN        (* (reg8 *) POWER__SIO_HYST_EN)
    #define POWER_SIO_REG_HIFREQ     (* (reg8 *) POWER__SIO_REG_HIFREQ)
    #define POWER_SIO_CFG            (* (reg8 *) POWER__SIO_CFG)
    #define POWER_SIO_DIFF           (* (reg8 *) POWER__SIO_DIFF)
#endif /* (POWER__SIO_CFG) */

/* Interrupt Registers */
#if defined(POWER__INTSTAT)
    #define POWER_INTSTAT            (* (reg8 *) POWER__INTSTAT)
    #define POWER_SNAP               (* (reg8 *) POWER__SNAP)
    
	#define POWER_0_INTTYPE_REG 		(* (reg8 *) POWER__0__INTTYPE)
#endif /* (POWER__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_POWER_H */


/* [] END OF FILE */

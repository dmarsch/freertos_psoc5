/*******************************************************************************
* File Name: Update_Timer_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "Update_Timer.h"

static Update_Timer_backupStruct Update_Timer_backup;


/*******************************************************************************
* Function Name: Update_Timer_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Update_Timer_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void Update_Timer_SaveConfig(void) 
{
    #if (!Update_Timer_UsingFixedFunction)
        Update_Timer_backup.TimerUdb = Update_Timer_ReadCounter();
        Update_Timer_backup.InterruptMaskValue = Update_Timer_STATUS_MASK;
        #if (Update_Timer_UsingHWCaptureCounter)
            Update_Timer_backup.TimerCaptureCounter = Update_Timer_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!Update_Timer_UDB_CONTROL_REG_REMOVED)
            Update_Timer_backup.TimerControlRegister = Update_Timer_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: Update_Timer_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Update_Timer_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Update_Timer_RestoreConfig(void) 
{   
    #if (!Update_Timer_UsingFixedFunction)

        Update_Timer_WriteCounter(Update_Timer_backup.TimerUdb);
        Update_Timer_STATUS_MASK =Update_Timer_backup.InterruptMaskValue;
        #if (Update_Timer_UsingHWCaptureCounter)
            Update_Timer_SetCaptureCount(Update_Timer_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!Update_Timer_UDB_CONTROL_REG_REMOVED)
            Update_Timer_WriteControlRegister(Update_Timer_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: Update_Timer_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Update_Timer_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void Update_Timer_Sleep(void) 
{
    #if(!Update_Timer_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(Update_Timer_CTRL_ENABLE == (Update_Timer_CONTROL & Update_Timer_CTRL_ENABLE))
        {
            /* Timer is enabled */
            Update_Timer_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            Update_Timer_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    Update_Timer_Stop();
    Update_Timer_SaveConfig();
}


/*******************************************************************************
* Function Name: Update_Timer_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  Update_Timer_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Update_Timer_Wakeup(void) 
{
    Update_Timer_RestoreConfig();
    #if(!Update_Timer_UDB_CONTROL_REG_REMOVED)
        if(Update_Timer_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                Update_Timer_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */

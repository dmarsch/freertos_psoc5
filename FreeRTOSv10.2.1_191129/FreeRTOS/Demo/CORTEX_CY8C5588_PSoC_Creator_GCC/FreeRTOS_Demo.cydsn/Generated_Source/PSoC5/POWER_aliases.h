/*******************************************************************************
* File Name: POWER.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_POWER_ALIASES_H) /* Pins POWER_ALIASES_H */
#define CY_PINS_POWER_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define POWER_0			(POWER__0__PC)
#define POWER_0_INTR	((uint16)((uint16)0x0001u << POWER__0__SHIFT))

#define POWER_INTR_ALL	 ((uint16)(POWER_0_INTR))

#endif /* End Pins POWER_ALIASES_H */


/* [] END OF FILE */

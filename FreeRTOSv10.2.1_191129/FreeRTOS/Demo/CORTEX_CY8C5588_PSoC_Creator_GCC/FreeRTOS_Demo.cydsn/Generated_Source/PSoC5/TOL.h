/*******************************************************************************
* File Name: TOL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_TOL_H) /* Pins TOL_H */
#define CY_PINS_TOL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "TOL_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 TOL__PORT == 15 && ((TOL__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    TOL_Write(uint8 value);
void    TOL_SetDriveMode(uint8 mode);
uint8   TOL_ReadDataReg(void);
uint8   TOL_Read(void);
void    TOL_SetInterruptMode(uint16 position, uint16 mode);
uint8   TOL_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the TOL_SetDriveMode() function.
     *  @{
     */
        #define TOL_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define TOL_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define TOL_DM_RES_UP          PIN_DM_RES_UP
        #define TOL_DM_RES_DWN         PIN_DM_RES_DWN
        #define TOL_DM_OD_LO           PIN_DM_OD_LO
        #define TOL_DM_OD_HI           PIN_DM_OD_HI
        #define TOL_DM_STRONG          PIN_DM_STRONG
        #define TOL_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define TOL_MASK               TOL__MASK
#define TOL_SHIFT              TOL__SHIFT
#define TOL_WIDTH              1u

/* Interrupt constants */
#if defined(TOL__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in TOL_SetInterruptMode() function.
     *  @{
     */
        #define TOL_INTR_NONE      (uint16)(0x0000u)
        #define TOL_INTR_RISING    (uint16)(0x0001u)
        #define TOL_INTR_FALLING   (uint16)(0x0002u)
        #define TOL_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define TOL_INTR_MASK      (0x01u) 
#endif /* (TOL__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define TOL_PS                     (* (reg8 *) TOL__PS)
/* Data Register */
#define TOL_DR                     (* (reg8 *) TOL__DR)
/* Port Number */
#define TOL_PRT_NUM                (* (reg8 *) TOL__PRT) 
/* Connect to Analog Globals */                                                  
#define TOL_AG                     (* (reg8 *) TOL__AG)                       
/* Analog MUX bux enable */
#define TOL_AMUX                   (* (reg8 *) TOL__AMUX) 
/* Bidirectional Enable */                                                        
#define TOL_BIE                    (* (reg8 *) TOL__BIE)
/* Bit-mask for Aliased Register Access */
#define TOL_BIT_MASK               (* (reg8 *) TOL__BIT_MASK)
/* Bypass Enable */
#define TOL_BYP                    (* (reg8 *) TOL__BYP)
/* Port wide control signals */                                                   
#define TOL_CTL                    (* (reg8 *) TOL__CTL)
/* Drive Modes */
#define TOL_DM0                    (* (reg8 *) TOL__DM0) 
#define TOL_DM1                    (* (reg8 *) TOL__DM1)
#define TOL_DM2                    (* (reg8 *) TOL__DM2) 
/* Input Buffer Disable Override */
#define TOL_INP_DIS                (* (reg8 *) TOL__INP_DIS)
/* LCD Common or Segment Drive */
#define TOL_LCD_COM_SEG            (* (reg8 *) TOL__LCD_COM_SEG)
/* Enable Segment LCD */
#define TOL_LCD_EN                 (* (reg8 *) TOL__LCD_EN)
/* Slew Rate Control */
#define TOL_SLW                    (* (reg8 *) TOL__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define TOL_PRTDSI__CAPS_SEL       (* (reg8 *) TOL__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define TOL_PRTDSI__DBL_SYNC_IN    (* (reg8 *) TOL__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define TOL_PRTDSI__OE_SEL0        (* (reg8 *) TOL__PRTDSI__OE_SEL0) 
#define TOL_PRTDSI__OE_SEL1        (* (reg8 *) TOL__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define TOL_PRTDSI__OUT_SEL0       (* (reg8 *) TOL__PRTDSI__OUT_SEL0) 
#define TOL_PRTDSI__OUT_SEL1       (* (reg8 *) TOL__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define TOL_PRTDSI__SYNC_OUT       (* (reg8 *) TOL__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(TOL__SIO_CFG)
    #define TOL_SIO_HYST_EN        (* (reg8 *) TOL__SIO_HYST_EN)
    #define TOL_SIO_REG_HIFREQ     (* (reg8 *) TOL__SIO_REG_HIFREQ)
    #define TOL_SIO_CFG            (* (reg8 *) TOL__SIO_CFG)
    #define TOL_SIO_DIFF           (* (reg8 *) TOL__SIO_DIFF)
#endif /* (TOL__SIO_CFG) */

/* Interrupt Registers */
#if defined(TOL__INTSTAT)
    #define TOL_INTSTAT            (* (reg8 *) TOL__INTSTAT)
    #define TOL_SNAP               (* (reg8 *) TOL__SNAP)
    
	#define TOL_0_INTTYPE_REG 		(* (reg8 *) TOL__0__INTTYPE)
#endif /* (TOL__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_TOL_H */


/* [] END OF FILE */
